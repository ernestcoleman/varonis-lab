[CmdletBinding()]
Param(
    [Parameter(Position = 0, Mandatory = $false, HelpMessage = 'Azure Credentials')]
    [Alias('Cred')]
    [PSCredential]$Credential
    )

# Create log file
$Logfile = $MyInvocation.MyCommand.Path -replace '\.ps1$', '.log'
$dt = get-date -Format "MM-dd-yyyy hh:mm"
# Start Logging
Start-Transcript -Path $Logfile

function Install-AzureAD {
    try {
        Import-Module -Name AzureAD -ErrorAction Stop
    }
    catch {
        Install-Module -Name AzureAD -AllowClobber -Repository PSGallery -Force
    } 
}

# Connect to Azure AD

if (!(Get-Module -ListAvailable -Name "AzureAD")){
    Install-AzureAD
}
try {
    Write-Verbose "$dt Connecting to Azure AD."
    Connect-AzureAD -Credential $Credential -ErrorAction Stop | Out-Null
    Write-Verbose "$dt Connected!"
}
catch {
    Write-Verbose "$dt Unable to connect to Azure AD, please check credentials and/or connection."
    break
}   


function Add-AzureADGroup {
    $GroupName = "Varonis Assignment Group"
    $azureadgroup = Get-AzureADGroup -All $true | Where-Object {$_.DisplayName -eq "$GroupName"}
    if(!$azureadgroup){
        New-AzureADGroup -DisplayName $GroupName -MailEnabled $false -SecurityEnabled $true -MailNickName "Null"| Out-Null
        Write-Verbose "$dt Group: '$GroupName' has been created."
    }
    else {
        Write-Verbose "$dt Group: '$GroupName' already exists."
    }
}


$PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
$PasswordProfile.Password = Read-Host -Prompt "Enter desired Password" -AsSecureString
$PasswordProfile.EnforceChangePasswordPolicy = 1
$PasswordProfile.ForceChangePasswordNextLogin = 1

$DomainName = "@ernestc0outlook.onmicrosoft.com"
$UserName = "Test User"
$MailNickName = "testuser"

Add-AzureADGroup

for ($i=1; $i -le 20; $i=$i+1 ) {
    $UserPrincipalName = "$MailNickName$i$DomainName"
    $azureaduser = Get-AzureADUser -All $true | Where-Object {$_.UserPrincipalName -eq "$UserPrincipalName"}
    if(!$azureaduser){
        try {
            New-AzureADUser -DisplayName "$UserName $i" `
            -PasswordProfile $PasswordProfile `
            -UserPrincipalName $UserPrincipalName `
            -MailNickName $MailNickName$i `
            -AccountEnabled $true | Out-Null
            Write-Verbose "$dt User: '$UserName $i' account has been created successfully!"
        }
        catch {
            Write-Error "$dt Error occurred while creating Azure AD Account for user '$UserName $i'. $_"
        }
    }
    else {
        Write-Verbose "$dt User: '$UserName $i' already exists, skipping add user"
    }
    $ADUser = Get-AzureADUser -ObjectId "$UserPrincipalName"
    $ADUserDisplayName = $ADUser.DisplayName
    $ADGroup = Get-AzureADGroup -SearchString "Varonis Assignment Group"
    $GroupDisplayName = $ADGroup.DisplayName
    $ADGroupMembers = Get-AzureADGroupMember -ObjectId $ADGroup.ObjectId | Where-Object { $_.ObjectId -eq $ADuser.ObjectId}
    if(!$ADGroupMembers) {
        try{
            Add-AzureADGroupMember -ObjectId $ADGroup.ObjectId -RefObjectId $ADUser.ObjectId
            Write-Verbose "$dt User '$ADUserDisplayName'  has been added to group $GroupDisplayName successfully!"
        }
        catch {
            Write-Error "$dt Error occurred while adding AD User '$ADUserDisplayName' to Group $GroupDisplayName. $_"
        }
    }
    else {
        Write-Verbose "$dt User: '$UserPrincipalName' already exists in group"
    }
}

Stop-Transcript